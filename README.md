# Text Completion: "Computer Mum"
This is a project experimenting with using GPT-2 for text completion in the context of memes. Much of the code was adapted from [this very useful notebook](https://colab.research.google.com/github/sarthakmalik/GPT2.Training.Google.Colaboratory/blob/master/Train_a_GPT_2_Text_Generating_Model_w_GPU.ipynb) and [blog post](https://minimaxir.com/2019/09/howto-gpt2/) by [Max Woolf](https://minimaxir.com/).

## Background
My friend Lucy runs a meme account on Instagram: [goose.fm](https://www.instagram.com/goose.fm/). These memes generally take the form of a picture overlaid with pink text beginning "Mum says". She got in touch with me to ask whether it would be possible for a bot to generate the text, having been trained on her existing memes. I replied that I should be able to achieve this via fine-tuning a large language model: a new task for me, but I was up for the challenge!

Here are some examples of Lucy's memes:

<p float="left">
  <img title="Mum says crypto is hot right now" alt="A picture of three children is overlaid with the text 'Mum says crypto is hot right now'" src="/images/crypto.jpg" width="400" />
  <img title="Mum says you are epic weekend" alt="A picture of a family of astronauts on a mountainous planet is overlaid with the text 'Mum says you are epic weekend'" src="/images/epic_weekend.jpg" width="400" /> 
  <img title="Mum says read the news" alt="A picture of a man buying a newspaper from another man is overlaid with the text 'Mum says read the news join a union use ur voice'" src="/images/read_the_news.jpg" width="400" />
  <img title="Mum says stay mad" alt="A picture of a woman in medieval attire with fists raised is overlaid with the text 'Mum says stay mad'" src="/images/stay_mad.jpg" width="400" />
</p>

You can see that they are idiosyncratic in their use of slang, topical subjects and sheer creativity. A very interesting emulation task for AI!

## Warning
Large language models such as GPT-2 have been trained on vast amounts of text from the internet, including offensive content. **It is a well-known problem with such models that the offensive nature of some of the training data can be repeated in model outputs.** Whilst this project is for fun, please be aware that some of the generated text may be offensive in nature due to this known issue. This does not reflect Lucy or her views, nor does it reflect me or mine. It does highlight the risks and the implications of using such large language models in everyday applications. 

## Data
The training data comes in the form of a text file - a list of previous "Mum says" meme texts. It is a comparatively small dataset for the task of finetuning a large language model, but is just large enough to train the model chosen here.

## Usage of Repository
To make changes use a feature branch `feature/new_fancy_thing` and merge to main.

### Commit Strings
Ideally commit strings should start with the following prefixes:
* INIT: Changes to repo structure/Makefiles/.gitignores etc
* DOCS: Updates to READMEs, notes, and design images
* LINT: Spelling or syntax changes
* TEST: If you've just written test code
* FUNC: Functional change to code (can also fold in TEST, REFACTOR and DOCS if they are small)
* REFACTOR: Making code clean and DRY without changing functionality
* CICD: Changes to pipelines
